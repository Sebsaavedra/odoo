# -*- utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import datetime
import logging
import string
import re

_logger = logging.getLogger(__name__)

from itertools import cycle
from odoo import api, models, fields
from odoo.tools.misc import ustr
from odoo.exceptions import ValidationError

class ResPartner(models.Model):
    _inherit = 'res.partner'
    _vat = 'vat'
    vat = fields.Char(string='RUT', required=True, translate=True, store=True, compute_sudo=False)
    
    _logger.info('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
    
    @api.onchange('vat', 'street2')
    def _onchange_vat(self):
        _logger.info('/////////////////////////////////')
        self.update_document()
    
    @api.constrains('vat')
    def check_vatnumber(self):
        for record in self:
            obj = self.search([('vat', '=', record.vat)])
            if len(obj) > 0:
                raise Warning('ATENCIÓN, EL RUT INGRESADO, YA EXISTE. INTENTE NUEVAMENTE.')
            else:
                return True
    
    @api.one
    def update_document(self):
        _logger.info('---------------------------------')
        print "###########"
	
	self.vat = 'toto'
	
        if not self.vat:
            return False
        
	raise ValidationError('RUT invalido')
	rut = self.vat.upper()
        rut = rut.replace("-","")
        rut = rut.replace(".","")
        aux = rut[:-1]
        dv = rut[-1:]
	
        revertido = map(int, reversed(str(aux)))
        factors = cycle(range(2,8))
        s = sum(d * f for d, f in zip(revertido,factors))
        res = (-s)%11
	
        if str(res) == dv:
            return True
        elif dv=="K" and res==10:
            return True
        else:
            return False

